package debugging;

import java.util.Arrays;

public class Tableau {

	private int[] tab;
	public static void main(String[]args) {

		int[] t= {4,1,1,3,4};

		Tableau tab=new Tableau(t);

		System.out.println(tab.getMode());

		testGetMode();

	}

	public Tableau(int[] tab) {

		super();

		this.tab = tab;

	}

	public int getMode() {

		int ret =-1;
		int nbMax=0;
		int nbOcc=0;

		// if ((this.tab.length>0)&&(this.tab!=null)){
		/*
		 * On inverse les conditions car Java les vérifie de gauche à droite.
		 * Si on passe un tableau null et qu'on esaye d'en trouver la longueur (this.tab.length),
		 * on aura une NullPointerException.
		 * On vérifie donc d'abord que le tableau n'est pas null, puis qu'il n'est pas vide.
		 */
		if ((this.tab!=null)&&(this.tab.length>0)){

			ret=this.tab[0];

			for (int i=0;i<this.tab.length;i++){

				nbOcc=1;
				
				// for (int j=i;j<=this.tab.length;j++){
				/*
				 * On vérifie que j est strictement inférieur à la longueur du tableau (this.tab.length)
				 * pour éviter une ArrayIndexOutOfBoundsException.
				 * On vérifie aussi que j est bien supérieur à i pour éviter de compter deux fois
				 * le même nombre.
				 */
				for (int j=i+1;j<this.tab.length;j++){

					if(this.tab[i]==this.tab[j]){

						nbOcc++;

					}

				}

				// if (nbOcc>=nbMax){
				/*
				 * On enlève le = pour ne garder que le premier nombre maximum d'occurences.
				 * Si on laisse le =, on aura le dernier nombre maximum d'occurences.
				 */
				if (nbOcc>nbMax){

					nbMax=nbOcc;

					// ret=i;
					/*
					 * On met tab[i] pour récupérer la valeur du tableau à l'indice i.
					 * Si on met i, on aura l'indice de la valeur du tableau.
					 */
					ret=tab[i]; // ret = i;

				}

			}

			System.out.println("Le nombre maximum d'occurence est :"+nbMax);

		} else {

			System.out.println("Ce calcul est impossible");

		}

		return ret;

	}

	public static void testGetMode() {

		System.out.println("*** Testing getMode() ***\n");

		System.out.println("Normal cases :\n");

		int[] t1 = {4,1,1,3,4};
		System.out.print("\t" + Arrays.toString(t1) + ".getMode() -> ");
		testCaseGetMode(new Tableau(t1), 4);

		int[] t2 = {4,1,1,3,4,3,3,3,3,3,3,3,3,3,3,3,3};
		System.out.print("\t" + Arrays.toString(t2) + ".getMode() -> ");
		testCaseGetMode(new Tableau(t2), 3);

		int[] t3 = {1,2,3,4,5,6,7,8,9,10};
		System.out.print("\t" + Arrays.toString(t3) + ".getMode() -> ");
		testCaseGetMode(new Tableau(t3), 1);

		System.out.println("Edge cases :\n");

		int[] t4 = {1};
		System.out.print("\t" + Arrays.toString(t4) + ".getMode() -> ");
		testCaseGetMode(new Tableau(t4), 1);

		int[] t5 = {};
		System.out.print("\t" + Arrays.toString(t5) + ".getMode() -> ");
		testCaseGetMode(new Tableau(t5), -1);

		int[] t6 = null;
		System.out.print("\t" + Arrays.toString(t6) + ".getMode() -> ");
		testCaseGetMode(new Tableau(t6), -1);

	}

	public static void testCaseGetMode(Tableau tab, int expected) {
	
		int res = tab.getMode();

		System.out.print("\tExpected : " + expected + " | Got : " + res);

		if (res == expected) {

			System.out.println(" -> Test Passed ✅\n");

		} else {

			System.out.println(" -> Test Failed ❌\n");

		}

	}

}