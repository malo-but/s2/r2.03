import org.junit.*; // accès aux classes JUnit4
import org.junit.runner.*; // permet l’exécution de la classe de test
import static org.junit.Assert.*; // accès aux assertions

public class TestMoney {

	@Test
	public void constructorTest() {
		System.out.println("\nConstructor test - normal cases :");
		this.constructorTestCase(10, "EUR");
		this.constructorTestCase(0, "EUR");
		this.constructorTestCase(-10, "EUR");

		System.out.println("\nConstructor test - error cases :");
		this.constructorTestCase(10, null);
	}

	private void constructorTestCase(int amount, String currency) {
		boolean passed = true;
		try {
			System.out.print("Money(" + amount + ", " + currency + ") => ");
			new Money(amount, currency);
		} catch (IllegalArgumentException e) {
			passed = false;
			System.out.println("Test FAILURE ❌ : " + e.getMessage());
		}

		if (passed)
			System.out.println("Test SUCCESS ✅");
	}

	@Test
	public void addTest() {
		System.out.println("\nAdd test - normal cases :");

		Money e10 = new Money(10, "EUR");
		Money e0 = new Money(0, "EUR");
		Money e20 = new Money(20, "EUR");

		this.addTestCase(e10, e10, e20);
		this.addTestCase(e10, e0, e10);
		this.addTestCase(e10, new Money(-10, "EUR"), e0);

		System.out.println("\nAdd test - error cases :");
		this.addTestCase(e10, e0, e20);
		this.addTestCase(e10, new Money(10, "USD"), null);
		this.addTestCase(e10, null, null);
	}

	private void addTestCase(Money money1, Money money2, Money expected) {
		boolean passed = true;
		try {
			System.out.print(money1 + " + " + money2 + " => ");
			Money res = money1.add(money2);
			try {
				assertEquals("", expected.equals(res), true);
			} catch (AssertionError e) {
				passed = false;
				System.out.println("Test FAILURE ❌ : Wrong result");
			}
		} catch (IllegalArgumentException e) {
			passed = false;
			System.out.println("Test FAILURE ❌ : " + e.getMessage());
		} catch (BadCurrencyException e) {
			passed = false;
			System.out.println("Test FAILURE ❌ : " + e.getMessage());
		}

		if (passed)
			System.out.println("Test SUCCESS ✅");
	}

	@Test
	public void equalsTest() {
		System.out.println("\nEquals test - normal cases :");

		Money e10 = new Money(10, "EUR");
		Money e0 = new Money(0, "EUR");

		this.equalsTestCase(e10, e10, true);
		this.equalsTestCase(e10, e0, false);
		this.equalsTestCase(e10, new Money(-10, "EUR"), false);
		this.equalsTestCase(e10, new Money(10, "USD"), false);

		System.out.println("\nEquals test - error cases :");
		this.equalsTestCase(e10, null, false);	
	}

	private void equalsTestCase(Money money1, Money money2, boolean expected) {
		boolean passed = true;
		boolean res = false;
		try {
			System.out.print(money1 + " == " + money2 + " : ");
			res = money1.equals(money2);
			try {
				assertEquals("", expected, res);
			} catch (AssertionError e) {
				passed = false;
				System.out.println("Test FAILURE ❌ : Wrong result");
			}
		} catch (IllegalArgumentException e) {
			passed = false;
			System.out.println("Test FAILURE ❌ : " + e.getMessage());
		}

		if (passed)
			System.out.println(res + " => Test SUCCESS ✅");
	}
	
	public static void main(String args[]) {
		JUnitCore.main("TestMoney");
	}

}
