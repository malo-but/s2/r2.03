public class Money {
	
	private int amount;

	private String currency;

	public Money(int amount, String currency) throws IllegalArgumentException {
		this.amount = amount;

		if (currency == null) {
			throw new IllegalArgumentException("Currency cannot be null");
		}
		this.currency = currency;
	}

	public int getAmount() {
		return amount;
	}

	public String getCurrency() {
		return currency;
	}

	public Money add(Money money) throws BadCurrencyException, IllegalArgumentException {
		if (money == null)
			throw new IllegalArgumentException("Money cannot be null");

		if (!this.currency.equals(money.getCurrency()))
			throw new BadCurrencyException("Wrong currency");

		return new Money(this.amount + money.getAmount(), this.currency);
	}

	public boolean equals(Money money) throws IllegalArgumentException {
		if (money == null)
			throw new IllegalArgumentException("Money cannot be null");

		return this.amount == money.getAmount() && this.currency.equals(money.getCurrency());
	}

	public String toString() {
		return this.amount + " " + this.currency;
	}

}
