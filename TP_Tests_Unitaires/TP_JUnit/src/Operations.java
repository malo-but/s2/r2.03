public class Operations {

	public int additionne(int premier, int second) {

		int ret = premier + second ;
		return ret ;

	}

	double calculeRacineCarree (double val) throws ArithmeticException {

		double ret = Math.sqrt(val) ;
		if (Double.isNaN(ret))
			throw new ArithmeticException("Impossible de calculer la racine carrée.") ;
		return ret ;	

	}

}