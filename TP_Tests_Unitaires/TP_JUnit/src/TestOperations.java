import org.junit.*; // accès aux classes JUnit4
import org.junit.runner.*; // permet l’exécution de la classe de test
import static org.junit.Assert.*; // accès aux assertions

public class TestOperations {

	private Operations op;

	// méthode exécutée systématiquement AVANT chaque cas de test
	@Before
	public void instancier() {
		this.op = new Operations();
	}

	// Premier cas de test : test de la méthode additionne
	@Test
	public void testAdditionne() {
		System.out.println ( "\nTest de la méthode additionne - cas normal" );
		int res = this.op.additionne( 1, 2 );
		// automatisation : « res » doit contenir « 3 » sinon le test est en erreur
		// if/else s’écrit en 1 seule ligne avec assertEquals
		assertEquals ( "Echec du test", 3, res );
		System.out.println ( "Test réussi" );
	}

	@Test
	public void testCalculeRacineCarree() {
		System.out.println( "\nTest de la méthode calculeRacineCarree - cas normal" );
		this.testCasCalculeRacineCarree(4, 2);
		this.testCasCalculeRacineCarree(9, 3);
		this.testCasCalculeRacineCarree(16, 4);

		System.out.println( "\nTest de la méthode calculeRacineCarree - cas d'erreur" );
		this.testCasCalculeRacineCarree(4, 3);
		this.testCasCalculeRacineCarree(-1, Double.NaN);
		this.testCasCalculeRacineCarree(-2, 4);

	}

	private void testCasCalculeRacineCarree(double value, double expected) {
		boolean passed = true;
		try {

			double res = this.op.calculeRacineCarree(value);
			System.out.println( "Racine carrée de " + value + " = " + res);

			try {
				assertEquals ( "", expected, res, 0d );
			} catch ( AssertionError e ) {
				passed = false;
				System.out.println ("ECHEC du test : racine carrée incorrecte => obtenue : " + res + " au lieu de " + expected);
			} 

		} catch ( ArithmeticException e ) {
			passed = false;
			System.out.println ( "ECHEC du test : " + e.getMessage() );
		}

		if (passed) {
			System.out.println("Test réussi");
		}

	}

	// lanceur
	public static void main ( String args[]) {
		JUnitCore.main("TestOperations");
	}

}