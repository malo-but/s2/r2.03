import java.io.*;
import java.util.NoSuchElementException;
import java.util.InputMismatchException;

/**
 * Class copying the Scanner class
 */
public class MyScanner {

	/**
	 * The buffer reader
	 */
	private BufferedReader bufferReader;

	/**
	 * Boolean indicating if the scanner is closed
	 */
	private boolean isClosed;

	/**
	 * Constructor
	 * @param source the source
	 * @throws FileNotFoundException if the file is not found
	 */
	public MyScanner(File source) throws FileNotFoundException {

		this.isClosed = true;
		
		FileReader fr = new FileReader(source);

		this.bufferReader = new BufferedReader(fr);

		this.isClosed = false;
	}

	/**
	 * Constructor
	 * @param in the input stream
	 */
	public MyScanner(InputStream in) {
		
		this.isClosed = true;

		InputStreamReader isr = new InputStreamReader(in);

		this.bufferReader = new BufferedReader(isr);

		this.isClosed = false;
	}

	/**
	 * Method returning the next integer in the given input stream
	 * @return the next integer in the given input stream
	 * @throws IllegalStateException if the scanner is closed
	 * @throws NoSuchElementException if there is no next line
	 * @throws InputMismatchException if the input is not an integer
	 */
	public int nextInt() throws IllegalStateException, NoSuchElementException, InputMismatchException {
		
		String str = "";
		int res = 0;

		if (this.isClosed)
			throw new IllegalStateException("ERROR => Scanner is closed.");

		if (!this.hasNextLine())
			throw new NoSuchElementException("ERROR => No next line.");

		try {
			str = this.bufferReader.readLine().trim();
		} catch (IOException e) {
			System.out.println(e.getMessage());
		}

		try {
			res = Integer.parseInt(str);
		} catch (NumberFormatException e) {
			throw new InputMismatchException("ERROR => Input is not an integer.");
		}

		return res;

	}

	/**
	 * Method closing the scanner
	 */
	public void close() {

		if (this.isClosed)
			throw new IllegalStateException("ERROR => IO problem encountered.");
		try {
			this.bufferReader.close();
		} catch (IOException e) {
			System.out.println(e.getMessage());
		}

		this.isClosed = true;
	}

	/**
	 * Method returning the next line in the given file
	 * @return the next line in the given file
	 * @throws IllegalStateException if the scanner is closed
	 * @throws NoSuchElementException if there is no next line
	 */
	public String nextLine() throws IllegalStateException, NoSuchElementException {

		String res = "";

		if (this.isClosed)
			throw new IllegalStateException("ERROR => Scanner is closed.");

		if (!this.hasNextLine())
			throw new NoSuchElementException("ERROR => No next line.");
		else 
			try {
				res = this.bufferReader.readLine();
			} catch (IOException e) {
				System.out.println(e.getMessage());
			}

		return res;

	}

	/**
	 * Method checking if there is a next line in the given file
	 * @return true if there is a next line in the given file, false otherwise
	 * @throws IllegalStateException if the scanner is closed
	 */
	public boolean hasNextLine() throws IllegalStateException {

		if (this.isClosed)
			throw new IllegalStateException();

		boolean hasNext = false;

		try {

            this.bufferReader.mark(100);

            if (this.bufferReader.read() < 0) {
                hasNext = false;
            } else {
                this.bufferReader.reset();
                hasNext = true;
            }
        } catch (IOException e) {
            System.out.println(e.getMessage());
        }

		return hasNext;

	}

}