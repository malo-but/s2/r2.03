import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.NoSuchElementException;
import java.util.InputMismatchException;

/**
 * Class testing the MyScanner class
 */
public class TestMyScanner {

	/**
	 * Main method
	 * @param args the arguments
	 */
	public static void main(String[] args) {
		new TestMyScanner();
	}

	/**
	 * Constructor
	 */
	public TestMyScanner() {
		this.testConstructeur();
		this.testClose();
		this.testHasNextLine();
		this.testNextLine();
		this.testNextInt();
	}

	/**
	 * Tests the constructor
	 */
	private void testConstructeur() {

		System.out.println("*** TEST DU CONSTRUCTEUR ***\n");

		String file = "../test.txt";

		System.out.println("Cas Normal avec \"" + file + "\" :");

		try {
			new MyScanner(new File(file));
		} catch (FileNotFoundException e) {
			System.out.println(e.getMessage());
		}

		System.out.println("Constructeur OK.");

		file = "../teesst.txt";

		System.out.println("\nCas d'erreur avec \"" + file + "\" :");

		try {
			new MyScanner(new File(file));
		} catch (FileNotFoundException e) {
			System.out.println(e.getMessage());
			return;
		}

		System.out.println("Constructeur OK.");

	}

	/**
	 * Tests the close method
	 */
	private void testClose() {

		System.out.println("\n*** TEST DE CLOSE() ***\n");

		String file = "../test.txt";

		System.out.println("Cas Normal avec \"" + file + "\" :");

		MyScanner sc = null;

		try {
			sc = new MyScanner(new File(file));
		} catch (FileNotFoundException e) {
			System.out.println(e.getMessage());
		}

		sc.close();

		try {
			sc.nextLine();
		} catch (IllegalStateException e) {
			System.out.println(e.getMessage());
		}

		System.out.println("Close OK.");

	}

	/**
	 * Tests the hasNextLine method
	 */
	private void testHasNextLine() {

		System.out.println("\n*** TEST DE HASNEXTLINE() ***\n");

		String file = "../test.txt";

		System.out.println("Cas Normal avec \"" + file + "\" :");

		MyScanner sc = null;

		try {
			sc = new MyScanner(new File(file));
		} catch (FileNotFoundException e) {
			System.out.println(e.getMessage());
		}

		int lineCount = this.countLines(file);

		System.out.println("\"" + file + "\" a " + lineCount + " lignes.");

		for (int i = 0; i < lineCount; i++) {
			sc.nextLine();
			System.out.println("Ligne " + (i+1) + " = hasNextLine() => " + sc.hasNextLine());
		}

	}
	
	/**
	 * Tests the nextLine method
	 */
	private void testNextLine() {

		System.out.println("\n*** TEST DE NEXTLINE() ***\n");

		String file = "../test.txt";

		System.out.println("Cas Normal avec \"" + file + "\" :");

		MyScanner sc = null;

		try {
			sc = new MyScanner(new File(file));
		} catch (FileNotFoundException e) {
			System.out.println(e.getMessage());
		}

		int i = 0;

		while (sc.hasNextLine()) {
			i++;
			String line = sc.nextLine();
			if (line.equals("ligne n°" + i))
				System.out.println("\"" + line + "\" => Ligne " + i + " OK.");
			else
				System.out.println(line + " => Ligne " + i + " ERREUR.");
		}

		System.out.println("\nCas d'Erreur avec \"" + file + "\" :");

		try {
			sc.nextLine();
		} catch (NoSuchElementException e) {
			System.out.println(e.getMessage());
		}

		try {
			sc.close();
			sc.nextLine();
		} catch (IllegalStateException e) {
			System.out.println(e.getMessage());
		}


	}

	/**
	 * Tests the nextInt method
	 */
	private void testNextInt() {

		System.out.println("\n*** TEST DE NEXTINT() ***\n");

		System.out.println("Cas Normal :");

		MyScanner sc = new MyScanner(System.in);

		System.out.println("Saisir un entier :");

		int i = sc.nextInt();

		System.out.println("Entier saisi : " + i);

		System.out.println("\nCas Limite :");

		System.out.println("Saisir un entier en ajoutant des espaces avant et après :");

		try {
			i = sc.nextInt();
		} catch (InputMismatchException e) {
			System.out.println(e.getMessage());
		}

		System.out.println("Entier saisi : " + i);

		System.out.println("\nCas d'Erreur :");

		System.out.println("Saisir une chaine de caractères :");

		try {
			i = sc.nextInt();
		} catch (InputMismatchException e) {
			System.out.println(e.getMessage());
		}

		System.out.println("Fermerture du scanner...");

		sc.close();

		System.out.println("Saisir un entier :");

		try {
			i = sc.nextInt();
		} catch (IllegalStateException e) {
			System.out.println(e.getMessage());
		}

	}
	
	/**
	 * Counts the number of lines in a file 
	 * @param fileName the file name
	 * @return the number of lines in the file
	 */
	private int countLines(String fileName) {

		Path path = Paths.get(fileName);
  
		int lines = 0;
		try {
			lines = (int)Files.lines(path).count();
		} catch (IOException e) {
			e.printStackTrace();
		}
  
		return lines;
	}

}
