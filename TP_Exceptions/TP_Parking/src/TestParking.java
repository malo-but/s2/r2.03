/**
 * Class testing the Parking class
 */
public class TestParking {

	/**
	 * Main method
	 * @param args the arguments
	 */
	public static void main(String[] args) {
		TestParking.testConstructeurEtToString();
		TestParking.testGarer();
		TestParking.testSortir();
	}

	/**
	 * Tests the constructor and the toString method
	 */
	public static void testConstructeurEtToString() {

		System.out.println("*** testConstructeurEtToString() ***\n");

		Parking parking = new Parking();

		System.out.println(parking);

	}

	/**
	 * Tests the garer method
	 */
	public static void testGarer() {

		System.out.println("*** testGarer() ***\n");

		Parking parking = new Parking();

		parking.garer(new Voiture("Ford", "Mustang", 315), 1);

		try {
			parking.garer(new Voiture("Renault", "Kangoo", 70), 67);
		} catch (ArrayIndexOutOfBoundsException e) {
			System.out.println(e.getMessage());
		}
		
		try {
			parking.garer(new Voiture("Ferrari", "Enzo", 400), 1);
		} catch (RuntimeException e) {
			System.out.println(e.getMessage() + "\n");
		}

		System.out.println(parking);

	}

	/**
	 * Tests the sortir method
	 */
	public static void testSortir() {

		System.out.println("*** testSortir() ***\n");

		Parking parking = new Parking();

		parking.garer(new Voiture("Ford", "Mustang", 315), 1);

		parking.garer(new Voiture("Renault", "Twizy", 30), 3);

		parking.sortir(1);

		try {
			parking.sortir(56);
		} catch (ArrayIndexOutOfBoundsException e) {
			System.out.println(e.getMessage());
		}

		try {
			parking.sortir(2);
		} catch (RuntimeException e) {
			System.out.println(e.getMessage() + "\n");
		}

		System.out.println(parking);

	}

}
