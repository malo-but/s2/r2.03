/**
 * Class testing the Voiture class
 */
public class TestVoiture {

	/**
	 * Main method
	 * @param args the arguments
	 */
	public static void main(String[] args) {
		TestVoiture.testConstructeurEtToString();
	}

	/**
	 * Tests the constructor and the toString method
	 */
	static void testConstructeurEtToString() {

		System.out.println("*** Voiture(\"Renault\", \"Clio\", 90) = ");
		Voiture v1 = new Voiture("Renault", "Clio", 90);
		System.out.println(v1);

		System.out.println("\n*** Voiture(\"Peugeot\", \"C15\", 70) = ");
		Voiture v2 = new Voiture("Peugeot", "C15", 70);
		System.out.println(v2);

		System.out.println("\n*** Voiture(null, \"C3\", 60) = ");
		Voiture v3 = new Voiture(null, "C3", 60);
		System.out.println(v3);

		System.out.println("\n*** Voiture(\"Ford\", null, 300) = ");
		Voiture v4 = new Voiture("Ford", null, 300);
		System.out.println(v4);

		System.out.println("\n*** Voiture(\"Fiat\", \"Multipla\", -1) = ");
		Voiture v5 = new Voiture("Fiat", "Multipla", -1);
		System.out.println(v5);

		System.out.println("\n*** Voiture(null, null, 0) = ");
		Voiture v6 = new Voiture(null, null , 0);
		System.out.println(v6);

	}
	
}
