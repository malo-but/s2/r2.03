/**
 * Class representing a car
 */
public class Voiture {
	
	/**
	 * The brand of the car
	 */
	private String marque;

	/**
	 * The model of the car
	 */
	private String modele;

	/**
	 * The power of the car
	 */
	private int puissance;

	/**
	 * Constructor
	 * @param marque the brand of the car
	 * @param modele the model of the car
	 * @param puissance the power of the car
	 */
	public Voiture(String marque, String modele, int puissance) {

		if (marque != null)
			this.marque = marque;
		else
			this.marque = "default";

		if (modele != null)
			this.modele = modele;
		else
			this.modele = "default";

		if (puissance > 0)
			this.puissance = puissance;
		else
			this.puissance = 1;

	}

	/**
	 * Returns a string representation of the car
	 */
	public String toString() {
		return "Marque : " + this.marque + "\nModele : " + this.modele + "\nPuissance : " + this.puissance;
	}

}