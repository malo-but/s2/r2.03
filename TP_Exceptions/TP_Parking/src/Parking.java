/**
 * Class representing a parking
 */
public class Parking {
	
	/**
	 * Number of places in the parking
	 */
	private static final int NB_PLACES = 4;

	/**
	 * Array of cars
	 */
	private Voiture[] lesPlaces;

	/**
	 * Constructor
	 */
	public Parking() {
		this.lesPlaces = new Voiture[NB_PLACES];
	}

	/**
	 * Returns a string representation of the parking
	 */
	public String toString() {

		String res = "PARKING :\n\n";

		for (Voiture v : this.lesPlaces) {

			if (v == null) {
				res += "Place Libre\n";
			} else {
				res += "\n" + v.toString() + "\n\n";
			}

		}

		return res;
	}

	/**
	 * Checks if the number of the place is valid
	 * @param numPlace the number of the place
	 * @throws ArrayIndexOutOfBoundsException if the number of the place is not valid
	 */
	private void numeroValide(int numPlace) throws ArrayIndexOutOfBoundsException {
		if (numPlace < 0 || numPlace > NB_PLACES) {
			throw new ArrayIndexOutOfBoundsException("Numéro de place non valide");
		}
	}

	/**
	 * Parks a car in the parking
	 * @param voit the car to park
	 * @param numPlace the number of the place
	 * @throws RuntimeException if the place is already occupied
	 */
	public void garer(Voiture voit, int numPlace) throws RuntimeException {

		this.numeroValide(numPlace);

		if (this.lesPlaces[numPlace] != null) 
			throw new RuntimeException("Place occupée");

		this.lesPlaces[numPlace] = voit;

	}

	/**
	 * Removes a car from the parking
	 * @param numPlace the number of the place
	 * @return the car removed
	 * @throws RuntimeException if there is no car at this place
	 */
	public Voiture sortir(int numPlace) throws RuntimeException {

		this.numeroValide(numPlace);

		if (this.lesPlaces[numPlace] == null)
			throw new RuntimeException("Pas de voiture à cette place");

		Voiture res = this.lesPlaces[numPlace];

		this.lesPlaces[numPlace] = null;

		return res;

	}

}
