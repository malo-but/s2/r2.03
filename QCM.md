# Points d'arrêt
> ligne non executée

# Points d'arrêt conditionnel
> break lorsque condition rencontrée

# 3 fenêtres 
> variables
> breakpoints
> expressions

# 4 types de pas
> step-over
> step-into
> step-return
> resume 

# Exceptions

## Non contrôlées

> Subclass de Error
> Subclass de RuntimeException 

## Contrôlées

> Tout le reste
### Doit être catch ou throw pour compiler
